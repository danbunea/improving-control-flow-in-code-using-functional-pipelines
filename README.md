Code for article: [How to solve it? Complexity in code - Improving flow control through functional pipelines](https://danbunea.blogspot.com/2019/09/how-to-solve-it-complexity-in-code.html)

****How to solve it? Complexity in code - Improving flow control through functional pipelines****


Code: https://gitlab.com/danbunea/improving-control-flow-in-code-using-functional-pipelines

Summary:

Can you go from code like:

![ifs](resources/images/ifs.png) 

to code like:

![pipe](resources/images/flow.gif) 


**Why?**

`- Why?
- Because code is better
- Code is better? How?
- Because is it easier to: 
* read
* extend
* debug
- Prove it!
- Ok let's look at the following problem:`

**In practice**

We have to write an endpoint which returns an offer by its id:

`GET /offer-by-id/:offer-id
Possible results:

- 200 {"offer-id":2, "offer-data":""}
- 400 {"errors":["The id you provided is invalid"}]}
- 404 {"errors":["The id you provided cannot be found"}]}
IMPORTANT: if an id is valid but not found the deposit must be notified!
`
6 solutions with pretty pics while also being able to handle errors:

With Either:

![ifs](resources/images/either error-1.gif) 

With flags:

![ifs](resources/images/functional flag.gif) 

With overlow pipeline:

![ifs](resources/images/pipe overflow.gif) 


Are they any good?

Let’s look at:

1. **Readability**

In terms of readability the code as a pipeline is like:

step 1
then step 2
then step 3

While also handling the errors:

step 1
then step 2
then step 3
fail on-error

or 

safe(
   step 1
   then step 2
   then step 3
)

2. **Extendability by changing the requirements**

We now have to modify our endpoint

1. to check if the offers are still active. If the aren't we need to return an error
2. to update the number of times the offer has been accessed


GET /offer-by-id/:offer-id

Possible results:

- 200 {"offer-id":2, "offer-data":"", "active":true, "requests":1}
- 400 {"errors":["The id you provided is invalid"}]}
- 400 {"errors":["The offer expired"}]}
- 404 {"errors":["The id you provided cannot be found"}]}


IMPORTANT: if an id is valid but the offer expired the deposit must be notified!


3. **Debugging**

how is debugging done

**Real world examples**

Seeing examples of usage in the real world in front end js/cljs or on the backend in python and java. 

And **conclusions**. For these you might want to have a look yourself here: [How to solve it? Complexity in code - Improving flow control through functional pipelines](https://danbunea.blogspot.com/2019/09/how-to-solve-it-complexity-in-code.html). 



**Installing**


> npm install  

will install all the node modules necesary (mocha)

> npm test

to run the tests once.

> npm run watch

to run the tests every time you save a source/test file.