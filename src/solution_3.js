let {notify} = require('./commons');

let repository = {
  "200": {
    "offer-id": 200,
    "percentage": 15
  },
  "400": {
    "offer-id": 400,
    "percentage": 22
  }
};




class IResponseStategy {
  process() {};
}
class ValidationFailedResponse extends IResponseStategy {
  process() {
    return {
      "code": 400,
      "body": {
        "errors": ["The id you provided is invalid"]
      }
    }
  };
}
class NotFoundResponse extends IResponseStategy {
  process() {
    notify("Offer not found: " + this.id);
    return {
      "code": 404,
      "body": {
        "errors": ["The id you provided cannot be found"]
      }
    }
  };
}

class ValidResponse extends IResponseStategy {
  constructor(id) {
    super();
    this.id = id;
  }
  process() {
    return {
      "code": 200,
      "body": repository[this.id]
    }
  };
}

function chooseStategy(id) {
  let strategy = null;
  if (typeof id !== 'string') strategy = new ValidationFailedResponse();
  else if (!repository[id]) strategy = new NotFoundResponse();
  else strategy = new ValidResponse(id);
  return strategy;
}

function getOfferById(requestId) {
  return JSON.stringify(chooseStategy(requestId)
                        .process());
}




module.exports = {
  getOfferById:getOfferById
};




