let {notify} = require('./commons');


let repository = {
  "200": {
    "offer-id": 200,
    "percentage": 15,
    "active": true,
    "requests": 0
  },
  "400": {
    "offer-id": 400,
    "percentage": 22,
    "active": false,
    "requests": 0
  }
};





class Error {
  constructor(message) {
    this.message = message;
  }
}
class ValidationError extends Error {}
class NotFoundError extends Error {}
class ExpiredError extends Error {}

function validId(id) {
  if (typeof id !== 'string') throw new ValidationError("The id you provided is invalid");
  return id;
}

function findOfferById(id) {
  if (!repository[id]) {
    notify("Offer not found: "+id);
    throw new NotFoundError("The id you provided cannot be found");
  }
  if (!repository[id].active) {
    notify("Offer expired: "+id);
    throw new ExpiredError("The offer expired");
  }
  return repository[id];
}

function getOfferById(requestId) {
  let response = null;
  try {
    const id = validId(requestId);
    const offer = findOfferById(id);
    offer.requests++;
    response = {
      code: 200,
      body: offer
    };
  } catch (err) {
    if (err instanceof ValidationError) {
      response = {
        code: 400,
        body: {
          errors: [err.message]
        }
      };
    } else if (err instanceof ExpiredError) {
      response = {
        code: 400,
        body: {
          errors: [err.message]
        }
      };
    } else if (err instanceof NotFoundError) {
      response = {
        code: 404,
        body: {
          errors: [err.message]
        }
      };
    }
  }
  return JSON.stringify(response);
}


module.exports = {
  getOfferById:getOfferById
};




